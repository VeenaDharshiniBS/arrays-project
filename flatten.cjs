function flatten(elements,depth)
{

  if(!Array.isArray(elements))
    return [];

    if(depth === undefined)
        depth = 1;
    let ans = [];

    function helper(elements,depth)
    {
        for(let index=0;index<elements.length;index++)
        {
            if(Array.isArray(elements[index]) && depth>=1)
                helper(elements[index],depth-1);
            else
            {
                if(elements[index] !== undefined)
                    ans.push(elements[index]);
            }
                
        }
    }
    helper(elements,depth);
    return ans;
}

module.exports = flatten;