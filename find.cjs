function find(elements,cb)
{
    if(!Array.isArray(elements) || typeof cb != 'function')
        return undefined;

    for(let index=0; index<elements.length; index++)
    {
        if(cb(elements[index],index,elements) == true)
            return elements[index];
    }
    return undefined;
}

module.exports = find;