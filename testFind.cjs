const find = require('./find.cjs');

const arr = [1,2,6,90,7];
const students = [{id:1, name:"Anil"},
                {id:2, name:"Veena"},
                {id:3, name:"Divya"}];

console.log(find(arr,(ele)=>ele<90));
console.log(find(students,(student)=>student.id==3));
console.log(find((ele)=>ele<90));
console.log(arr.find((ele)=>ele<90));
console.log(students.find((student)=>student.id==3));
console.log(arr.find((ele)=>ele>90));