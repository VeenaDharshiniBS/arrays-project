const each = require('./each.cjs');

each([1,2,3,4,5],(x) => console.log(x));
each("hi",(x) => console.log(x));

const array1 = [1,2,3,4,5];
array1.forEach(ele => console.log(ele));
//"hi".forEach(ele => console.log(ele));
