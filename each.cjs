function each(elements, cb)
{
    if(!Array.isArray(elements) || typeof cb != 'function')
        return;

    for (let index=0; index<elements.length; index++) 
    {
        cb(elements[index],index,elements);
    }
}

module.exports = each;
