const flatten = require('./flatten.cjs');

const nestedArray = [1, [2], [[3]], [[[4]]]];

console.log(flatten("ghjd"));
console.log(flatten(nestedArray,2));
console.log(nestedArray.flat(2));
console.log(flatten([0, 1, , 2, [[[3, 4]]]],2));
console.log([0, 1, , 2, [[[3, 4]]]].flat(2));