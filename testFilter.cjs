const filter = require('./filter.cjs');

const arr = [1,2,6,90,7];
const students = [{id:1, name:"Anil"},
                {id:2, name:"Veena"},
                {id:3, name:"Divya"}];

console.log(filter(arr,(ele)=>ele<90));
console.log(filter(students,(student)=>student.id<3));
console.log(filter((ele)=>ele<90));
console.log(filter(arr));
console.log(arr.filter((ele)=>ele<90));
console.log(students.filter((student)=>student.id<3));