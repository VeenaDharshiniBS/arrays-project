function map(elements, cb) 
{
  if(!Array.isArray(elements) || typeof cb != 'function')
        return [];

  let ans = [];
  for ( let index=0; index<elements.length; index++)
  {
    ans.push(cb(elements[index],index,elements));
  }
  return ans;
}

module.exports = map;
