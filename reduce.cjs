function reduce(elements,cb,startingValue)
{
    if(!Array.isArray(elements) || typeof cb != 'function' || elements.length == 0)
        return [];
    
    if(startingValue === undefined)
    {
        startingValue = elements[0];
    }
    else
    {
        startingValue = cb(startingValue,elements[0],0,elements);
    }
        

    for(let index=1;index<elements.length; index++)
    {
        startingValue = cb(startingValue,elements[index],index,elements);
    }
    return startingValue;
}

module.exports = reduce;