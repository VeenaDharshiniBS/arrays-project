function filter(elements,cb)
{
    if(!Array.isArray(elements) || typeof cb != 'function')
        return [];
    let ans = [];
    for(let index=0; index<elements.length; index++)
    {
        if(cb(elements[index],index,elements) == true)
            ans.push(elements[index]);
    }
    return ans;
}

module.exports = filter;