const reduce = require('./reduce.cjs');

const elements = [1,2,3,4,5];
console.log(reduce(elements,(acc,curr)=>acc+curr,10));
console.log(reduce(elements,(acc,curr)=>acc+curr));
console.log(reduce(elements));
console.log(reduce((acc,curr)=>acc+curr,10));

console.log(elements.reduce((acc,curr)=>acc+curr,10));
console.log(elements.reduce((acc,curr)=>acc+curr));