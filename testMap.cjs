const map = require('./map.cjs');

const elements = [1,2,3,4,5];
console.log(map(elements,(value) => value+10 ));
console.log(map(elements,"fgh"));
console.log(elements.map((value) => value+10));
console.log(map(elements));
console.log(map(elements,(value)=>parseInt(value)));
console.log(elements.map((value)=>parseInt(value)));